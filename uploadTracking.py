#!/usr/bin/env python
# -*- encoding: utf-8 -*-
""" This app connects to both Shipstation (SS) and Channel Advisor (CA)
	to synchronize traking numbers for cross-account orders (eg: Canadian orders placed on the US account)
	for which tracking number is not properly uploaded to CA by SS.
	The app needs to be run manually or can be scheduled as a cronjob.
	No parameters are necessary.
	The config.yml file must be properly filled. Complete the config.yml.sample with your info.
	Do not share this file! It contains your api keys and secret, which must be kept SECRET.
"""
import requests
import yaml
import sys
import logging
import datetime
import re

ONE_WEEK_AGO = (datetime.datetime.now() - datetime.timedelta(days=7)).isoformat()

from base64 import b64encode

#Initiate logger and creates log files
logger = logging.getLogger('myapp')
hdlr = logging.FileHandler(str(datetime.date.today()) + ".log")
formatter = logging.Formatter('%(asctime)s %(levelname)s %(message)s')
hdlr.setFormatter(formatter)
logger.addHandler(hdlr)
#Change to logging.WARNING or ERROR for production
logger.setLevel(logging.DEBUG)

"""
	Loads the config file, the file must follow the format shown in the included config.yml.sample
"""
def loadConfig(config_file="config.yml"):
	with open(config_file, 'r') as ymlfile:
		cfg = yaml.load(ymlfile)
	return cfg

"""
	This class connects to the Channel Advisor REST API to get order information and mark them as shipped
"""
class channelIntegrator:
	def __init__(self):
		self.getAuth()

	def getAuth(self):
		""" POSTS necessary authentication info to the API and updates the access token"""
		body = {"grant_type": "refresh_token", "refresh_token": cfg["channel_advisor"]["refresh_token"], "access_type": "offline"}
		resp = requests.post("https://api.channeladvisor.com/oauth2/token", body, auth=(cfg["channel_advisor"]["app_id"], cfg["channel_advisor"]["shared_secret"]))
		self.access_token = resp.json()["access_token"]
		logger.info("Received New Channel Advisor Authentication Token")

	def getOrderPages(self):
		"""Builds the API url and return the response."""
		body = {"access_token": self.access_token}
		body["$filter"] = "PaymentStatus eq 'Cleared' and PaymentDateUTC ge %sZ" % ONE_WEEK_AGO
		body["$select"] = "ID, SiteID, SiteOrderID, PaymentStatus"
		body["$expand"] = "Fulfillments($select=TrackingNumber)"

		logger.info("Initial api request")
		resp = requests.get("https://api.channeladvisor.com/v1/orders", body)
		logger.info(resp.status_code)
		yield resp

		while "@odata.nextLink" in resp.json():
			url = resp.json()["@odata.nextLink"]
			logger.debug("Sent Request to %s", re.sub(r'(access_token=).*?(&\$)',r'\g<1>'+ "XXXXXXXXX" + r'\g<2>',url))
			resp = requests.get(url)
			logger.debug(resp.status_code)
			#If status code is error (usually because authentication is expired), reauthenticate and retry
			if 400 <= resp.status_code < 500:
				logger.warning("%s, reauthenticating" % resp.status_code)
				self.getAuth()
				url = re.sub(r'(access_token=).*?(&\$)',r'\g<1>'+ self.access_token + r'\g<2>',url)
				resp = self.requestProductPage(url)
				logger.debug(resp.status_code)
			yield resp

	def getOrdersWithoutTracking(self):
		"""Iterate through all orders on all pages and get the ones without a tracking number"""
		self.noTracking = []
		#iterate over each order in each page
		for orderPage in self.getOrderPages():
			for order in orderPage.json()["value"]:
				if len(order["Fulfillments"]) == 0 or "TrackingNumber" not in order["Fulfillments"][0] or order["Fulfillments"][0]["TrackingNumber"] is None:
					self.noTracking.append(order)
		logger.info("Got %d orders without tracking numbers" % len(self.noTracking))
		return self.noTracking

	def uploadShippingInfo(self, shippingInfo):
		"""Uploads shipment information to Channel Advisor for a specific order
		Builds the API url and return the response."""
		url = "https://api.channeladvisor.com/v1/Orders(" + str(shippingInfo["orderNumber"]) + ")/Ship"
		body = {"Value": {
			"ShippedDateUtc": shippingInfo["shipDate"],
			"TrackingNumber": shippingInfo["trackingNumber"],
			"DeliveryStatus": "Complete",
			"DistributionCenterID": 0 #We only have one actual DC, the others are FBA
		    }
		}
		headers = {
		    "Authorization": "Bearer %s" % self.access_token,
		    "Content-Type": "application/json"
		}
		resp = requests.post(url, body, headers=headers)
		if resp.status_code == 204:
			logger.debug("Uploaded tracking for Order # %s" % str(shippingInfo["orderNumber"]))
		else:
			logger.error("Could not upload tracking for Order # %s got a %d" %(str(shippingInfo["orderNumber"]), resp.status_code))
		return resp

"""
	This class connects to the Shipstation REST API to get shipment information for specific orders
"""
class shipstationIntegrator:
	def __init__(self):
		self.todaysOrders = None
		self.getAuth()

	def getAuth(self):
		"""Encodes key and secret as per Shipstation requirements outlined here: https://www.shipstation.com/developer-api/#/introduction/shipstation-api-requirements/authentication"""
		self.authorization = b64encode(b"%s:%s" % (cfg["shipstation"]["key"].encode("UTF-8"), cfg["shipstation"]["secret"].encode("UTF-8")))
		logger.info("Generated Shipstation auth code")

	def getShipmentInfo(self, orderID):
		"""Gets the tracking number for the specified orderID
		Returns the tracking Number if it exists, otherwise, returns None"""
		url = "https://ssapi.shipstation.com/shipments?orderNumber=" + str(orderID)
		resp = requests.get(url, headers={"Authorization": b"Basic " + self.authorization})
		#If we get an error because call/minute limit has been exceeded, wait for it to reset (+5 sec for safety) and request again
		if resp.status_code == 429 and resp.headers["X=Rate-Limit-Remaining"] == '0':
			logger.warning("Call per minute limit exceded, waiting")
			time.sleep(int(resp.headers["X-Rate-Limit-Reset"]) + 5)
			logger.debug("Retrying " + re.sub(r'(access_token=).*?(&\$)',r'\g<1>'+ "XXXXXXXXX" + r'\g<2>',url))
			resp = requests.get(url, headers={"Authorization": b"Basic " + self.authorization})

		if len(resp.json()["shipments"]) > 0 and resp.json()["shipments"][0]["trackingNumber"] is not None:
			logger.info("Got shipment info for order %s" % str(orderID))
			shipmentInfo = {k: v for k,v in resp.json()["shipments"][0].items() if k in ["orderNumber", "trackingNumber", "serviceCode", "shipDate"]}
			return shipmentInfo
		else:
			logger.debug("No shipment info found for order %s" % str(orderID))
			return None

	def getCombinedShipmentInfo(self, orderID):
		"""Gets the tracking number for the specified combined orderID by searching in the error messages
		Returns the tracking Number if it exists, otherwise, returns None"""
		if self.todaysOrders == None:
			url = "https://ssapi.shipstation.com/shipments?shipDateStart=" + datetime.date.today().isoformat() + "&pageSize=500"
			resp = requests.get(url, headers={"Authorization": b"Basic " + self.authorization})
			logger.info("Getting today's orders")

			#If we get an error because call/minute limit has been exceeded, wait for it to reset (+5 sec for safety) and request again
			if resp.status_code == 429 and resp.headers["X=Rate-Limit-Remaining"] == '0':
				logger.warning("Call per minute limit exceded, waiting")
				time.sleep(int(resp.headers["X-Rate-Limit-Reset"]) + 5)
				logger.debug("Retrying " + re.sub(r'(access_token=).*?(&\$)',r'\g<1>'+ "XXXXXXXXX" + r'\g<2>',url))
				resp = requests.get(url, headers={"Authorization": b"Basic " + self.authorization})

			if len(resp.json()["shipments"]) > 0:
				logger.info("Got Today's orders")
				self.todaysOrders = [order for order in resp.json()["shipments"] if order["notifyErrorMessage"] is not None]

		shipmentInfo = [{k: v for k,v in order.items() if k in ["orderId", "trackingNumber", "serviceCode", "shipDate"]} for order in self.todaysOrders if str(orderID) in order["notifyErrorMessage"]]
		if len(shipmentInfo) == 1:
			logger.debug("Got combined shipping info for order %d" % orderID)
			shipmentInfo[0]["orderId"] = orderID
			return shipmentInfo[0]
		else:
			logger.debug("No combined shipment info found for order %s" % str(orderID))
			return None

if __name__ == "__main__":
	cfg = loadConfig()
	channel = channelIntegrator()
	shipstation = shipstationIntegrator()
	noTracking = channel.getOrdersWithoutTracking()

	trackingFound = []
	trackingNotFound = []
	for order in noTracking:
		shipmentInfo = shipstation.getShipmentInfo(order["ID"])
		if shipmentInfo is not None:
			trackingFound.append(shipmentInfo)

	for order in trackingNotFound:
		shipmentInfo = shipstation.getCombinedShipmentInfo(order["ID"])
		if shipmentInfo is not None:
			trackingFound.append(shipmentInfo)

#Wait for ok
	print("{} tracking found.".format(len(trackingFound)))
	print("Do you want to upload these orders?")
	confirm = input("Type y/Y to continue: ")
	if(confirm.lower() != 'y'):
		sys.exit("Stopped program, no orders uploaded")
	for order in trackingFound:
		r = channel.uploadShippingInfo(order)
		if r.status_code != 204:
			logger.error("Could not update order #{}".format(order["orderNumber"]))
