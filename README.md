Shipstation tracking Uploader for Channel Advisor
================

Uploads tracking number for cross-account orders in Shipstation-Channel Advisor
---------------------------------------------------------------------

Shipstation cannot upload shipping information for orders made across different accounts (e.g. someone from the US orders on a Canadian account).
This program gets all unshipped orders from ChannelAdvisor for a specified period (Default: Last 7 days).
It then checks if each of those orders has an associated shipment in Shipstation.
For every order that has been shipped in Shipstation, it uploads the shipment information to Channel Advisor and logs any error.

### Prerequisites:
* Install requirements. (They are rather common packages so you might already have them installed)

```shell
pip install -r requirements.txt
```

* Copy or rename the included `config.sample.yml` to `config.yml` and fill in your information.
  
    * For Channel Advisor, you can create a new app to get the necessary info in the [Developper Console](https://api.channeladvisor.com/DeveloperConsole/).  

    * For Shipstation, you can get the API Key and Secret in the [API settings](https://ss4.shipstation.com/#/settings/api).  
  
    * If you don't have a developper account on any of those 2 platforms, see the references at the bottom of this readme.  
  
* Change the logging level to *logging.WARNING* or *logging.ERROR* to get only relevant errors.
  
### Usage:
* Run the program. We recommend doing this at the end of the day after your shipping team is done so as not to interfere with order currently being processed.

```shell
	./uploadTracking.py
```

* Currently, no options are supported, everything is setup through the `config.yml` file.

* Check the logs to see if everything was successful. By default, the logs will be in the app folder.

### References:
* [Channel Advisor Developper Documentation](https://developer.channeladvisor.com/)

* [Shipstation Developper Documentation](https://shipstation.docs.apiary.io/)
